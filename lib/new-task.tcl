
    
set default_uom [parameter::get_from_package_key -package_key intranet-trans-invoices -parameter "DefaultPriceListUomID" -default 324]
    
# More then one option for a TM?
# Then we'll have to show a few more fields later.
set ophelia_installed_p [llength [info procs im_package_ophelia_id]]

# Localize the workflow stage directories
set locale "en_US"
set source [lang::message::lookup $locale intranet-translation.Workflow_source_directory "source"]
set trans [lang::message::lookup $locale intranet-translation.Workflow_trans_directory "trans"]
set edit [lang::message::lookup $locale intranet-translation.Workflow_edit_directory "edit"]
set proof [lang::message::lookup $locale intranet-translation.Workflow_proof_directory "proof"]
set deliv [lang::message::lookup $locale intranet-translation.Workflow_deliv_directory "deliv"]
set other [lang::message::lookup $locale intranet-translation.Workflow_other_directory "other"]
    
    
set bgcolor(0) " class=roweven"
set bgcolor(1) " class=rowodd"

# --------- Get a list of files "source_xx_XX" dir---------------
# $file_list is a sorted list of all files in "source_xx_XX":
set task_list [list]
    
# Get some basic information about our current project
db_1row project_info "
    select	project_type_id
    from	im_projects
    where	project_id = :project_id
"
    
    
# Get the sorted list of files in the directory
set files [lsort [im_filestorage_find_files $project_id]]
    
set project_path [cog::project::path -project_id $project_id]
set org_paths [split $project_path "/"]
set org_paths_len [llength $org_paths]
set start_index $org_paths_len

foreach file $files {
    
    # Get the basic information about a file
    ns_log Debug "file=$file"
    set file_paths [split $file "/"]
    set file_paths_len [llength $file_paths]
    set body_index [expr $file_paths_len - 1]
    set file_body [lindex $file_paths $body_index]
    
    # The first folder of the project - contains access perms
    set top_folder [lindex $file_paths $start_index]
    ns_log Debug "top_folder=$top_folder"
    
    # Check if it is the toplevel directory
    if {[string equal $file $project_path]} {
        # Skip the path itself
        continue
    }
    
    # determine the part of the filename _after_ the base path
    set end_path ""
    for {set i [expr $start_index+1]} {$i < $file_paths_len} {incr i} {
        append end_path [lindex $file_paths $i]
        if {$i < [expr $file_paths_len - 1]} { append end_path "/" }
    }
    
    # add "source_xx_XX" folder contents to file_list
    if {[regexp ${source} $top_folder]} {
        # append twice: for right and left side of select box
        lappend task_list $end_path
        lappend task_list $end_path
    }
}
set ctr 0
    
# -------------------- Add subheader for New Task  --------------------------
set task_table ""
# -------------------- Add an Asp Wordcount -----------------------
    
set target_language_option_list [db_list target_lang_options "
    select	'<option value=\"' || language_id || '\">' ||
        im_category_from_id(language_id) || '</option>'
        from	im_target_languages
        where	project_id = :project_id
    "]
set target_language_options [join $target_language_option_list "\n"]
    
if {[ad_parameter -package_id [im_package_translation_id] EnableAspTradosImport "" 0]} {
    
    append task_table "
    <form enctype=multipart/form-data method=POST action=/intranet-translation/trans-tasks/trados-upload>
    [export_form_vars project_id return_url]
    "

    # Prepare the list of importers.
    # Start with the hard-coded importers coming with ]po[
    set importer_option_list [list \
        [list trados "Trados (3.0 - 9.0)"] \
        [list transit "Transit (all)"] \
        [list freebudget "FreeBudget (4.0 - 5.0)"] \
        [list webbudget "WebBudget (4.0 - 5.0"] \
    ]
    set importer_sql "
        select	category
        from	im_categories
        where	category_type = 'Intranet Translation Task CSV Importer'
        and (enabled_p is null OR enabled_p = 't')
    "
    set default_wordcount_app [ad_parameter -package_id [im_package_translation_id] "DefaultWordCountingApplication" "" "trados"]
    db_foreach importers $importer_sql {
        lappend importer_option_list [list $category [lang::message::lookup "" intranet-translation.Importer_$category $category]]
    }
    
    set importer_options_html ""

    foreach row $importer_option_list {
        set importer [lindex $row 0]
        set importer_pretty [lindex $row 1]
        set selected ""
        if {$importer == $default_wordcount_app} { set selected "selected" }
            append importer_options_html "<option value=\"[lindex $row 0]\" $selected>$importer_pretty</option>\n"
        }
    
        append task_table "
        <tr $bgcolor([expr $ctr % 2])>
          <td><nobr><b>[lang::message::lookup "" intranet-translation.Add_CSV_Analysis "Add CSV File for Analysis"]</b></nobr></td>
    <td>
            <nobr>
            <select name=wordcount_application>
            $importer_options_html
            </select>
            "
        append task_table "<input type=hidden name='tm_integration_type_id' value='[im_trans_tm_integration_type_external]'>\n"
        append task_table [im_trans_task_type_select task_type_id $project_type_id]
    
        append task_table "
        <select name=target_language_id>
        <option value=\"\">[lang::message::lookup "" intranet-translation.All_languages "All Languages"]</option>
        $target_language_options
        </select>
            <input type=file name=upload_file size=30 value='*.csv'>
        <input type=submit value='[lang::message::lookup "" intranet-translation.Add_Wordcount "Add Wordcount"]' name=submit_trados>
        </form>
        </nobr>
      </td>
      <td>
        [im_gif -translate_p 1 help "Use the 'Browse...' button to locate your file, then click 'Open'.\nThis file is used to define the tasks of the project, one task for each line of the wordcount file."]
      </td>
    </tr>
    "
    incr ctr
}

if {[apm_package_installed_p "intranet-trans-trados"]} {
    append task_table "
    <tr $bgcolor([expr $ctr % 2])>
    <td>      <nobr><b>[lang::message::lookup "" intranet-translation.Add_File_Analysis "Add File for Analysis"]</b></nobr></td>
    <td>
    <form enctype=multipart/form-data method=POST action=/intranet-trans-trados/upload-file>
    [export_form_vars project_id return_url]
      <nobr>
      <select name=quote_p><option value=\"1\">[lang::message::lookup "" intranet-translation.Create_quote "Create / Replace quote"]</option>
      <option value=\"0\" selected>[lang::message::lookup "" intranet-translation.Dont_create_quote "Don't create / replace quote"]</option></select>
        <select name=target_language_id>
        $target_language_options
        </select>
    <input type=file name=upload_file size=30>

      <input type=submit value='[lang::message::lookup "" intranet-translation.Add_File_Analysis "Add File for Analysis"]' name=analyse_trados>
          </form>
          </nobr>
        </td>
        <td>
          [im_gif -translate_p 1 help "Upload a new source file for trados analysis. Use the 'Browse...' button to locate your file, then click 'Open'.\nThis file is used for analysis, resulting in one additional translation task per selected target language"]
        </td>
      </tr>"
      incr ctr
    append task_table "
    <tr $bgcolor([expr $ctr % 2])>
    <td><nobr><b>[lang::message::lookup "" intranet-translation.Add_Wordcount "Add Wordcount"]</b></nobr>
    </td><td>
    <form enctype=multipart/form-data method=POST action=/intranet-trans-trados/upload-analysis>
    [export_form_vars project_id return_url]
      <nobr>
      <select name=quote_p><option value=\"1\">[lang::message::lookup "" intranet-translation.Create_quote "Create / Replace quote"]</option>
      <option value=\"0\" selected>[lang::message::lookup "" intranet-translation.Dont_create_quote "Don't create / replace quote"]</option></select>
        <select name=target_language_id>
        <option value=\"\">[lang::message::lookup "" intranet-translation.All_languages "All Languages"]</option>
        $target_language_options
        </select>
        <input type=file name=upload_file size=30>
      <input type=submit value='[lang::message::lookup "" intranet-translation.Add_Wordcount "Add Wordcount"]' name=submit_trados>
          </form>
          </nobr>
        </td>
        <td>
          [im_gif -translate_p 1 help "Upload a Trados Analysis XML file to create tasks for the selected languages based on the analyis found in the XML."]
        </td>
      </tr>"
      incr ctr
      append task_table "
    <tr $bgcolor([expr $ctr % 2])>
    <td colspan=2><a href='[export_vars -base "/intranet-trans-trados/update-tasks" -url {{quote_p 0} {project_id}}]' class=button><b>Update Tasks from Trados</B></a></td></tr>
"
}
    
# -------------------- Ophelia or Not -----------------------
set ext [im_trans_tm_integration_type_external]
if {$ophelia_installed_p} {
    set integration_type_html [im_category_select "Intranet TM Integration Type" tm_integration_type_id $ext]
    set integration_type_html "<td>$integration_type_html</td>"
    set colspan 9
} else {
    set integration_type_html "<input type=hidden name=tm_integration_type_id value=$ext>"
    set colspan 8
}
    
set individual_task_table "
    <table border=0>
    <tr><td colspan=$colspan></td></br>
    
    <tr>
    <td colspan=$colspan class=rowtitle align=center>
    [lang::message::lookup "" intranet-translation.Add_Individual_Files "Add Individual Files"]
    </td>
    </tr>
    <tr>
    <td class=rowtitle align=center>
    [_ intranet-translation.Task_Action]
    </td>
      <td class=rowtitle align=center>
        [_ intranet-translation.Task_Name]
      </td>
      <td class=rowtitle align=center>
        [_ intranet-translation.Units]
      </td>
      <td class=rowtitle align=center>
        [_ intranet-translation.UoM]
      </td>
      <td class=rowtitle align=center>
        [_ intranet-translation.Task_Type]
      </td>
      <td class=rowtitle align=center>
        [lang::message::lookup "" intranet-translation.Target_Language "Target Language"]
      </td>
"
    
if {$ophelia_installed_p} {
    append individual_task_table "
      <td class=rowtitle align=center>
           [lang::message::lookup "" intranet-translation.Integration_Type "Integration"]
          </td>
    "
}
append individual_task_table "
    <td class=rowtitle align=center>
    [_ intranet-translation.Task_Action]
    </td>
    <td class=rowtitle align=center>&nbsp;</td>
</tr>
"
    
# -------------------- Add Task Manually --------------------------
append individual_task_table "
    
    <tr $bgcolor([expr $ctr % 2])>
    <form enctype=multipart/form-data action=/intranet-translation/trans-tasks/task-action method=POST>
    [export_form_vars project_id return_url]
    <td><nobr><b>[_ intranet-translation.New_Manual_Task]</b></nobr></td>
    <td><input type=text size=20 value=\"\" name=task_name_manual></td>
    <td><input type=text size=2 value=0 name=task_units_manual></td>
    <td>[im_category_select "Intranet UoM" "task_uom_manual" $default_uom]</td>
    <td>[im_trans_task_type_select task_type_manual $project_type_id]</td>
        <td>
            <select name=target_language_id>
            <option value=\"\">[lang::message::lookup "" intranet-translation.All_languages "All Languages"]</option>
            $target_language_options
            </select>
        </td>
    $integration_type_html
    <td><input type=submit value=\"[_ intranet-translation.New_Manual_Task]\" name=submit_add_manual></td>
    <td>[im_gif -translate_p 1 help "Add a \"manual\" task to the project. \n This task is not going to controled by the translation workflow."]</td>
    </form>
  </tr>"

incr ctr

# -------------------- Add Single File Manually --------------------------
append individual_task_table "
    <tr $bgcolor([expr $ctr % 2])>
      <form enctype=multipart/form-data action=/intranet-translation/trans-tasks/task-action method=POST>
        [export_form_vars project_id return_url]
    <td><nobr><b>[_ intranet-translation.New_Source_File]</b></nobr></td>
      <td><input type=file name=upload_file size=30 value='*.csv'></td>
      <td><input type=text size=2 value=0 name=task_units_file></td>
      <td>[im_category_select "Intranet UoM" "task_uom_file" $default_uom]</td>
      <td>[im_trans_task_type_select task_type_file $project_type_id]</td>
          <td>
              <select name=target_language_id>
              <option value=\"\">[lang::message::lookup "" intranet-translation.All_languages "All Languages"]</option>
              $target_language_options
              </select>
          </td>
      $integration_type_html
      <td><input type=submit value=\"[_ intranet-translation.New_Source_File]\" name=submit_upload_file></td>
      <td>[im_gif -translate_p 1 help "Add a file \"manually\" to the project and create a task for it. \n This task is not going to controled by the translation workflow."]</td>
      </form>
    </tr>
"
# -------------------- Add a new task for existing File  --------------------------

if {0 < [llength $task_list]} {
    append individual_task_table "
      <tr $bgcolor([expr $ctr % 2])>
        <form enctype=multipart/form-data action=/intranet-translation/trans-tasks/task-action method=POST>
        [export_form_vars project_id return_url]
        <td><nobr><b>[_ intranet-translation.New_Task_for_file]</b></nobr></td>
        <td>[im_select -translate_p 0 "task_name_file" $task_list]</td>
        <td><input type=text size=2 value=0 name=task_units_file></td>
        <td>[im_category_select "Intranet UoM" "task_uom_file" $default_uom]</td>
        <td>[im_trans_task_type_select task_type_file $project_type_id]</td>
            <td>
                <select name=target_language_id>
                <option value=\"\">[lang::message::lookup "" intranet-translation.All_languages "All Languages"]</option>
                $target_language_options
                </select>
            </td>
        $integration_type_html
        <td><input type=submit value=\"[_ intranet-translation.New_Task_for_file]\" name=submit_add_file></td>
        <td>[im_gif -translate_p 1 help "Add a task for an already uploaded File. \n Files need to be located in the \"source_xx\" folder to appear in the drop-down box on the left."]</td>
        </form>
      </tr>
    "
    incr ctr
}



# -------------------- Add Task CSV --------------------------
append individual_task_table "
    <tr $bgcolor([expr $ctr % 2])>
    <form enctype=multipart/form-data action=/intranet-translation/trans-tasks/task-action method=POST>
    [export_form_vars project_id return_url]
    <td><b>[_ intranet-translation.New_Tasks_from_csv]</b></td>
      <td><input type=file name=upload_file size=30 value='*.csv'></td>
      <td>See CSV<input type=hidden name=task_units_manual value=''</td>
      <td>[im_category_select "Intranet UoM" "task_uom_manual" $default_uom]</td>
      <td>[im_trans_task_type_select task_type_manual $project_type_id]</td>
          <td>
              <select name=target_language_id>
              <option value=\"\">[lang::message::lookup "" intranet-translation.All_languages "All Languages"]</option>
              $target_language_options
              </select>
          </td>
      $integration_type_html
      <td><input type=submit value=\"[_ intranet-translation.New_Tasks_from_csv]\" name=submit_csv_manual></td>
      <td>[im_gif -translate_p 1 help "Add a \"manual\" task to the project. \n This task is not going to controled by the translation workflow."]</td>
      </form>
    </tr>
"

append individual_task_table "
    </table>
    </form>
"
