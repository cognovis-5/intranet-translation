# /packages/intranet-translation/projects/new-quick.tcl
#
# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Purpose: form to quickly add a translation project
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2011-08-05
} {
    {project_type_id ""}
    {project_status_id:integer,optional}
    {company_id:integer}
    {parent_id ""}
    {project_nr ""}
    {project_name ""}
    {workflow_key ""}
    {return_url ""}
    target_language_ids:multiple,optional
}

if {![exists_and_not_null company_id]} {
    ad_returnredirect [export_vars -base "/intranet-translation/projects/new" -url {project_type_id parent_id project_nr project_name workflow_key}]
}
# -----------------------------------------------------------
# Defaults
# -----------------------------------------------------------
set user_id [ad_maybe_redirect_for_registration]

    
set perm_p 0
# Check if the user has admin rights on the parent_id
# to allow freelancers to add sub-projects
if {"" != $parent_id} {
    im_project_permissions $user_id $parent_id view read write admin
    if {$admin} { set perm_p 1 }
}
    
# Users with "add_projects" privilege can always create new projects...
if {[im_permission $user_id add_projects]} { set perm_p 1 } 
if {!$perm_p} { 
    ad_return_complaint "Insufficient Privileges" "
        <li>You don't have sufficient privileges to see this page."
    return
}
    
# --------------------------------------------
# Create Form
# --------------------------------------------

set form_id "project-ae"


set target_language_options [db_list_of_lists languages "select category,category_id from im_categories where category_type = 'Intranet Translation Language' and enabled_p = 't' and length(category)>2 order by sort_order,category"]

# Get the final company options
set final_company_options [db_list_of_lists final_companies "select company_name, company_id from im_companies where company_type_id in (select * from im_sub_categories(10247)) and company_status_id in (select * from im_sub_categories(46)) order by lower(company_name)"]
# Add optional
set final_company_options [concat [list [list "" ""]] $final_company_options]

# Get the contact options
set customer_group_id [im_customer_group_id]
set freelance_group_id [im_freelance_group_id]

set company_contacts_sql "
select DISTINCT
    im_name_from_user_id(u.user_id) as user_name,
    u.user_id
from
    cc_users u,
    group_distinct_member_map m,
    acs_rels ur
where
    u.member_state = 'approved'
    and u.user_id = m.member_id
    and m.group_id in (:customer_group_id, :freelance_group_id)
    and u.user_id = ur.object_id_two
    and ur.object_id_one = :company_id
    and ur.object_id_one != 0
    order by user_name
"

set company_contact_options [db_list_of_lists contacts $company_contacts_sql]
ad_form -name $form_id -html { enctype multipart/form-data } -action /intranet-translation/projects/new-quick -cancel_url $return_url -form {
    project_id:key
    project_type_id:text(hidden)
    {project_name:text(text),optional
        {label "Project Name"}
    }
    {company_id:integer(hidden) {value $company_id}}
    {project_nr:text(hidden) {value $project_nr}}
    {company_contact_id:text(select),optional 
        {label "[_ intranet-translation.Client_contact]"}
        {options "$company_contact_options"}
    }
    {source_language_id:text(select)
        {label "[_ intranet-translation.Source_Language]"}
        {options "$target_language_options"}
    }
    {target_language_ids:text(jq_multiselect)
        {label "[_ intranet-translation.Target_Languages]"}
        {options "$target_language_options"}
    }
}

# ---------------------------------------------------------------
# Deal with additional skills
# ---------------------------------------------------------------

if {[apm_package_installed_p "intranet-freelance"]} {
    # Append the skills attributes
    im_freelance_append_skills_to_form \
        -object_subtype_id $project_type_id \
        -form_id $form_id
}

if {[lsearch [template::form::get_elements $form_id] subject_area_id]<0} {
    ad_form -extend -name $form_id -form {
        {subject_area_id:integer(im_category_tree),optional
            {label "[_ intranet-translation.Subject_Area]"}
            {custom {category_type "Intranet Translation Subject Area" translate_p 1}}
        }
    }
}

ad_form -extend -name $form_id -form {
    {final_company_id:integer(select),optional
        {label "[_ intranet-translation.Final_User]"}
        {options "$final_company_options"}
    }
    {upload_file:file(file),optional
        {label "[_ intranet-translation.Source_File]"}
        {help_text "The Source file can be a zip folder with all source files. Analysis will be performed"}
    }

} -on_request {
    set project_nr [im_next_project_nr -customer_id $company_id -parent_id $parent_id]
    set project_name $project_nr
} -on_submit {

    # Permission check. Cases include a user with full add_projects rights,
    # but also a freelancer updating an existing project or a freelancer
    # creating a sub-project of a project he or she can admin.
    set perm_p 0
    
    # Check for the case that this guy is a freelance
    # project manager of the project or similar...
    im_project_permissions $user_id $project_id view read write admin
    if {$write} { set perm_p 1 }
    
    # Check if the user has admin rights on the parent_id
    # to allow freelancers to add sub-projects
    if {"" != $parent_id} {
        im_project_permissions $user_id $parent_id view read write admin
        if {$write} { set perm_p 1 }
    }

    # Users with "add_projects" privilege can always create new projects...
    if {[im_permission $user_id add_projects]} { set perm_p 1 } 

    if {!$perm_p} { 
        ad_return_complaint "Insufficient Privileges" "<li>You don't have sufficient privileges to see this page."
        ad_script_abort
    }
    
} -new_data {

    set target_language_ids [element get_values $form_id target_language_ids]

    set project_id [im_translation_create_project \
        -company_id $company_id \
        -project_name $project_name \
        -project_nr $project_nr \
        -project_type_id $project_type_id \
        -project_lead_id [ad_conn user_id] \
        -source_language_id $source_language_id \
        -target_language_ids $target_language_ids \
        -subject_area_id $subject_area_id \
        -final_company_id $final_company_id] 
    
    if {[info exists company_contact_id]} {
        db_dml update_project "update im_projects set company_contact_id = :company_contact_id where project_id = :project_id"
    }
    
    # Set project status to potential
    db_dml update_project_status "update im_projects set project_status_id = [im_project_status_potential] where project_id = :project_id"
    
    # ---------------------------------------------------------------
    # Make sure we have the skills
    # ---------------------------------------------------------------
    if {[apm_package_installed_p "intranet-freelance"]} {
        # Append the skills attributes
        im_freelance_store_skills_from_form \
            -object_subtype_id $project_type_id \
            -form_id $form_id \
            -object_id $project_id
            
            
        # ---------------------------------------------------------------
        # We need to store source and target language again
        # ---------------------------------------------------------------
        foreach lang $target_language_ids {
            im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_target_language] -skill_ids $lang
        }
        im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_source_language] -skill_ids $source_language_id
    }
    
    
    # ---------------------------------------------------------------
    # If we have a file, unpack it (if necessary)
    # And then send it to trados for analysis
    # ---------------------------------------------------------------
    if {[exists_and_not_null upload_file]} {
    
        set trados_server [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosServer"]
        
        if {$trados_server ne ""} {
            # Files need to go to originals
            set project_dir [cog::project::path -project_id $project_id]
            set source_dir "$project_dir/Trados/Originals"
        } else {
            # Save the file in the correct directory
            set locale "en_US"
            set source [lang::message::lookup $locale intranet-translation.Workflow_source_directory "source"]
            set source_language [im_category_from_id -translate_p 0 $source_language_id]
            
            set project_dir [cog::project::path -project_id $project_id]
            set source_dir "$project_dir/${source}_$source_language"
        }
        set tmp_filename [template::util::file::get_property tmp_filename $upload_file]
        set filename [template::util::file::get_property filename $upload_file]
        file copy $tmp_filename ${source_dir}/$filename
        if {[file extension $filename] eq ".zip"} {
            exec unzip -d ${source_dir} ${source_dir}/$filename
            file delete -force ${source_dir}/$filename
        }
        
        # Run the analysis
        if {$trados_server ne ""} {
            im_trans_trados_create_analysis -project_id $project_id
            set days [im_translation_processing_time -project_id $project_id -include_pm -update]
            set invoice_id [im_trans_invoice_create_from_tasks -project_id $project_id]
            set start_timestamp [db_string start_date "select to_char(now(),'YYYY-MM-DD HH24:MI') from dual"]
            im_translation_update_project_dates -project_id $project_id -start_timestamp $start_timestamp
        } 
    
    }
    
} -after_submit {
    
    # -----------------------------------------------------------------
    # Flush caches related to the project's information
    
    util_memoize_flush_regexp "im_project_has_type_helper.*"
    util_memoize_flush_regexp "db_list_of_lists company_info.*"
    
    # -----------------------------------------------------------------
    # Call the "project_create" or "project_update" user_exit
    
    im_user_exit_call project_create $project_id
    
    
    if {[info exists invoice_id]} {
        set return_url [export_vars -base "/intranet-invoices/view" -url {invoice_id}]
    } else {
        set return_url [export_vars -base "/intranet/projects/view" {project_id}]
    }
    
    ad_returnredirect $return_url
    ad_script_abort
}
