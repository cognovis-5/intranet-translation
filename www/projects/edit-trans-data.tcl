# /packages/intranet-translation/projects/edit-trans-data.tcl
#
# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Purpose: form to quickly add a translation project
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2011-08-05
} {
    project_id:integer
    {return_url ""}
}

set user_id [ad_maybe_redirect_for_registration]
set return_url [export_vars -base "/intranet/projects/view" {project_id}]

set include_source_language_country_locale [im_parameter -package_id [im_package_translation_id] SourceLanguageWithCountryLocaleP "" 0]

im_project_permissions $user_id $project_id view read write admin
if {!$write} {
    ad_return_complaint 1 "<li>[_ intranet-translation.lt_You_have_insufficient]"
    return
}

set project_nr_field_min_len [parameter::get -parameter ProjectNrMinimumLength -package_id [im_package_core_id] -default 5]
# --------------------------------------------
# Create Form
# --------------------------------------------

set form_id "project-ae"

set target_language_options [db_list_of_lists languages "select category,category_id from im_categories where category_type = 'Intranet Translation Language' and enabled_p = 't' and length(category)>2 order by sort_order,category"]

# Get the final company options
set final_company_options [db_list_of_lists final_companies "select company_name, company_id from im_companies where company_type_id in (select * from im_sub_categories(10247)) and company_status_id in (select * from im_sub_categories(46)) order by lower(company_name)"]
# Add optional
set final_company_options [concat [list [list "" ""]] $final_company_options]

# Get the contact options
set customer_group_id [im_customer_group_id]
set freelance_group_id [im_freelance_group_id]


ad_form -name $form_id -html { enctype multipart/form-data } -action /intranet-translation/projects/edit-trans-data -form {
    project_id:key
} -export {return_url}

# ------------------------------------------------------
# Dynamic Fields
# ------------------------------------------------------

set object_type "im_project"
set dynfield_project_type_id [im_opt_val project_type_id]
if {[info exists project_id]} {
    set existing_project_type_id [db_string ptype "select project_type_id from im_projects where project_id = :project_id" -default 0]
    if {0 != $existing_project_type_id && "" != $existing_project_type_id} {
        set dynfield_project_type_id $existing_project_type_id
    }
}

set dynfield_project_id 0
if {[exists_and_not_null project_id]} {
    set dynfield_project_id $project_id 
    set ::super_project_id $project_id
} else { 
    set ::super_project_id 0
}

im_dynfield::append_attributes_to_form \
    -object_subtype_id $dynfield_project_type_id \
    -object_type $object_type \
    -form_id $form_id \
    -object_id $dynfield_project_id 
 


if {[apm_package_installed_p "intranet-freelance"]} {
    # Append the skills attributes
    im_freelance_append_skills_to_form \
        -object_subtype_id $dynfield_project_type_id \
        -form_id $form_id \
        -object_id $dynfield_project_id 
} 

if {![info exists parent_id]} {
    set parent_id [db_string get_parent "select parent_id from im_projects where project_id = :project_id" -default "0"]
}

ad_form -extend -name $form_id -form {
    {source_language_id:text(select)
        {label "[_ intranet-translation.Source_Language]"}
        {options "$target_language_options"}
    }
    {target_language_ids:text(jq_multiselect)
        {label "[_ intranet-translation.Target_Languages]"}
        {options "$target_language_options"}
    }
} -on_submit {

    # Permission check. Cases include a user with full add_projects rights,
    # but also a freelancer updating an existing project or a freelancer
    # creating a sub-project of a project he or she can admin.
    set perm_p 0
    
    # Check for the case that this guy is a freelance
    # project manager of the project or similar...
    im_project_permissions $user_id $project_id view read write admin
    if {$write} { set perm_p 1 }
    
    # Users with "add_projects" privilege can always create new projects...
    if {[im_permission $user_id add_projects]} { set perm_p 1 } 

    if {!$perm_p} { 
        ad_return_complaint "Insufficient Privileges" "<li>You don't have sufficient privileges to see this page."
        ad_script_abort
    }

    if {[info exists project_nr]} {
        set project_nr [string tolower [string trim $project_nr]]
    }
            
} -validate {
    {project_nr
        {[string length $project_nr] >= $project_nr_field_min_len}
        {[lang::message::lookup "" intranet-core.lt_The_project_nr_that "The Project Nr is too short."] <br>
           [lang::message::lookup "" intranet-core.lt_Please_use_a_project_nr_ "Please use a longer Project Nr or modify the parameter 'ProjectNrMinimumLength'."]}
    }
    {project_nr
        {[string length $project_nr] <= 100}
        {[lang::message::lookup "" intranet-core.lt_The_project_nr_is_too_long "The Project Nr is too long."] <br>
           [lang::message::lookup "" intranet-core.lt_Please_use_a_shorter_project_nr_ "Please use a shorter Project Nr."]}
    }
    {project_name
        {[db_string project_name_exists "
    select  count(*)
    from    im_projects
    where   upper(trim(project_name)) = upper(trim(:project_name)) and
            project_id <> :project_id and
            (parent_id = :parent_id OR (:parent_id is null and parent_id is null))"] == 0}
        "[_ intranet-core.lt_The_specified_name_pr]"
    }
} -edit_request {
    set target_language_ids [im_target_language_ids $project_id]
    set source_language_id [db_string source "select source_language_id from im_projects where project_id = :project_id"]
    set company_id [db_string company_id "select company_id from im_projects where project_id = :project_id"]

    db_1row projects_info_query { 
    select 
            p.*,
            p.company_project_nr
    from
        im_projects p
    where 
        p.project_id = :project_id 
    }
    

} -edit_data {
    
    set sql "
                select
                        project_type_id         as previous_project_type_id,
                        company_id              as previous_project_company_id
                from    im_projects
                where   project_id = :project_id
        "
    if {![db_0or1row select_orig_values $sql] } {
        ad_return_complaint 1 "Could not find project with id: $project_id, please get in touch with your System Administrator"
    }
    
    set project_path $project_nr
     
    # Callback for checking the edited data
	cog::callback::invoke -action before_update -object_id $project_id

    # Store the dynfields (aka. all now)
    im_dynfield::attribute_store \
        -object_type $object_type \
        -object_id $project_id \
        -form_id $form_id
     
    # With the skills from the intranet-freelance module
    # it is possible to store them with the project
    if {[apm_package_installed_p "intranet-freelance"]} {
        # Append the skills attributes
        im_freelance_store_skills_from_form \
            -object_subtype_id $project_type_id \
            -form_id $form_id \
            -object_id $project_id
    }
        
    # Save the information about the project target languages
    # in the im_target_languages table
    db_dml delete_im_target_language "delete from im_target_languages where project_id=:project_id"

    set target_language_ids [element get_values $form_id target_language_ids]

    # Delete the target languages from the necessary skills
    if {[im_table_exists im_freelancers]} {
        db_dml delete "
        delete from im_object_freelance_skill_map
        where
            object_id = :project_id
            and skill_type_id = [im_freelance_skill_type_target_language]"       
    }

    foreach lang $target_language_ids {
        ns_log Notice "target_language=$lang"
        set sql "insert into im_target_languages values ($project_id, $lang)"
        db_dml insert_im_target_language $sql 
        if {[im_table_exists im_freelancers]} {
            im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_target_language] -skill_ids $lang
        }
    }
    

    # Add the source language, subject_area_id and expected_quality_id as a skill
    if {[im_table_exists im_freelancers]} {
        im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_source_language] -skill_ids $source_language_id
	db_dml update_Source_lang "update im_projects set source_language_id = :source_language_id where project_id = :project_id"
    }

    # ---------------------------------------------------------------------
    # Create the directory structure necessary for the project
    # ---------------------------------------------------------------------
    set fs_dir_create_proc [parameter::get -package_id [im_package_translation_id] -parameter "FilestorageCreateFolderProc"]
    $fs_dir_create_proc $project_id

    # -----------------------------------------------------------------
    # Call the "project_create" or "project_update" user_exit
    cog::callback::invoke -action after_update -object_id $project_id    
    
    # -----------------------------------------------------------------
    # add the creating current_user to the group
    
    if { [exists_and_not_null project_lead_id] } {
        im_biz_object_add_role $project_lead_id $project_id [im_biz_object_role_project_manager]
    }
    
    
    # Send a notification for this task
    set params [list  [list base_url "/intranet/projects/"]  [list project_id $project_id] [list return_url ""] [list no_write_p 1]]
    
    # ---------------------------------------
    # Close subprojects and tasks if needed
    # ---------------------------------------
    
    if {[im_category_is_a $project_status_id [im_project_status_closed]]} {
    
        # Find the list of tasks in all subprojects and close them
        # We might need to think about workflows in the future here!
        set close_task_ids [im_project_subproject_ids -project_id $project_id -type task]
        foreach close_task_id $close_task_ids {
            db_dml close_task "update im_timesheet_tasks set task_status_id = [im_timesheet_task_status_closed] where task_id = :close_task_id"
            db_dml close_task "update im_projects set project_status_id = [im_project_status_closed] where project_id = :close_task_id"
    
        }
        # Find the list of subprojects
        set close_subproject_ids [im_project_subproject_ids -project_id $project_id -exclude_self]
        foreach close_project_id $close_subproject_ids {
            db_dml close_task "update im_projects set project_status_id = :project_status_id where project_id = :close_project_id"
        }
    }
    
} -after_submit {
    
    # -----------------------------------------------------------------
    # Flush caches related to the project's information
    
    util_memoize_flush_regexp "im_project_has_type_helper.*"
    util_memoize_flush_regexp "db_list_of_lists company_info.*"
    
    ad_returnredirect $return_url
    ad_script_abort
}

# -----------------------------------------------------------
# NavBars
# -----------------------------------------------------------

# Setup the subnavbar
set bind_vars [ns_set create]
ns_set put $bind_vars project_id [im_opt_val project_id]
set parent_menu_id [db_string parent_menu "select menu_id from im_menus where label='project'" -default 0]
set menu_label "project_summary"
set sub_navbar [im_sub_navbar \
                    -base_url [export_vars -base "/intranet/projects/view" {project_id}] \
                    $parent_menu_id \
                    $bind_vars "" "pagedesriptionbar" $menu_label \
               ]
