# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Template for email inclusion
# @author Malte Sussdorff (sussdorff@sussdorff.de)
# @creation-date 2005-06-14

ad_page_contract {
    See if this person is authorized to read the task file,
    guess the MIME type from the original client filename and
    write the binary file to the connection

    @author malte.sussdorff@cognovis.de
    @creation-date 2017-07-08
} {
    type
    user_id:integer
    project_id:integer
    package_file
    target_language
    {return_url ""}
    {new_status_p 1}
}

set current_user_id [ad_maybe_redirect_for_registration]
set task_id [db_string task_id "select task_id from im_trans_tasks where ${type}_id = :user_id and project_id = :project_id limit 1" -default ""]
set page_title "Upload return package"

# Get the overall permissions
im_translation_task_permissions $current_user_id $task_id view read write admin

if {!$read} {
    ad_return_complaint 1 "<li>[_ intranet-translation.lt_You_have_insufficient_1]"
    return
}

set form_elements {
    {package_filename:text(inform),optional {label "[_ acs-mail-lite.Associated_files]"} {value $package_filename}}
    {package_file:text(hidden) {value $package_file}}
    {user_id:text(hidden) {value $user_id}}
    {upload_file:file(file)
        {label "[_ acs-mail-lite.Upload_file]"}
    }
    {project_id:text(hidden) {value $project_id}}
    {type:text(hidden) {value $type}}
    {target_language:text(hidden) {value $target_language}}
    {cancel_url:text(hidden) {value $return_url}}
}

set package_filename [file tail $package_file]
set edit_buttons [list [list [_ acs-mail-lite.Upload_file] Upload]]

ad_form -action [ad_conn url] \
    -html {enctype multipart/form-data} \
    -name package_upload \
    -cancel_label "[_ acs-kernel.common_Cancel]" \
    -cancel_url $return_url \
    -edit_buttons $edit_buttons \
    -form $form_elements \
    -on_submit {
	    
	    # Check that the uploaded filename matches
	    set upload_filepath [lindex $upload_file 1]
	    set upload_filename [lindex $upload_file 0]
	    if {[file extension $upload_filename] ne ".sdlrpx"} {
    	    ad_return_error "Extension Mismatch" "Your uploaded file $upload_filename does not seem to be a return package. Please go back and upload a correct package file"
	    }
	    set files_in_package_list [split [exec -- /usr/bin/unzip -Z -1 "$upload_filepath"] "\n"]
	    set matched_p 0
	    foreach file $files_in_package_list {
    	    if {[string match "${target_language}*" $file]} {
        	    set matched_p 1
        	    break
            }
	    }
	    if {!$matched_p} {
    	    ad_return_error "Language Missing" "Your package file $upload_filename does not seem to contain a folder for the target language ${target_language}. It is therefore not a valid return package for $package_filename"
	    }
	    
	    # Copy the file to the folder
	    
	    set upload_folder [im_trans_task_folder \
	       -project_id $project_id \
	       -target_language $target_language \
	       -folder_type $type \
	       -return_package]
    
        file rename -force $upload_filepath "[cog::project::path -project_id $project_id]/${upload_folder}/$upload_filename"

        if {$new_status_p} {
            db_foreach task "select task_id,task_type_id,task_status_id from im_trans_tasks where ${type}_id = :user_id and project_id = :project_id" {
                
                # Record the upload action
                im_trans_upload_action -upload_file $upload_filename $task_id $task_status_id $task_type_id $user_id
            }
        }
    } -after_submit {
        if {$cancel_url eq ""} {
            set cancel_url  [export_vars -base "/intranet/projects/view" -url {project_id}]
        }
        ad_returnredirect $cancel_url
    }

