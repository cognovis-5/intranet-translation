<master src="../../../intranet-core/www/master">
<property name="title">@page_title@</property>
<property name="main_navbar_label">projects</property>
<property name="sub_navbar">@sub_navbar;noquote@</property>
<link href="/intranet-jquery/jquery_datetimepicker/jquery.datetimepicker.css" media="screen" rel="stylesheet" type="text/css">
<script src="/intranet-jquery/jquery_datetimepicker/jquery.js" type="text/javascript"></script>
<script src="/intranet-jquery/jquery_datetimepicker/build/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

<if @auto_assignment_component_p@>
	@auto_assignment_html;noquote@
</if>
<if @mass_assignment_component_p@>
	@mass_assignment_html;noquote@
</if>

<if @task_html@ ne "">
	@task_html;noquote@
</if>
<else>
	<%=[lang::message::lookup "" intranet-translation.No_tasks_found "No tasks found"]%>
</else>

<p>  
	@price_html;noquote@
<p>