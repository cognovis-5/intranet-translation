# /packages/intranet-translation/tcl/intranet-translation-callback-procs.tcl
# Please check http://www.project-open.com/ for licensing
# details.

ad_library {
    Specific stuff for translation callbacks

    @author malte.sussdorff@cognovis.de
    
}

ad_proc -public -callback im_project_new_redirect -impl translation {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
    {-project_id:required}
    {-parent_id:required}
    {-company_id:required}
    {-project_type_id:required}
    {-project_name:required}
    {-project_nr:required}
    {-workflow_key:required}
    {-return_url:required}
} {
    Redurect if needed
} {
    # Returnredirect to translations for translation projects
    if {[im_category_is_a $project_type_id [im_project_type_translation]] && $project_id eq ""} {
        set quick_creation_url [parameter::get_from_package_key -package_key "intranet-translation" -parameter "QuickProjectCreationURL"]
        if {$quick_creation_url ne ""} {
            ad_returnredirect [export_vars -base "$quick_creation_url" -url {project_type_id project_status_id company_id parent_id project_nr project_name workflow_key return_url project_id}]
        } else {
            ad_returnredirect [export_vars -base "/intranet-translation/projects/new" -url {project_type_id project_status_id company_id parent_id project_nr project_name workflow_key return_url project_id}]
        }
    }
}

ad_proc -public -callback intranet-translation::task_status_package_file {
    -type:required
    -user_id:required
    -project_id:required
    -target_language_id:required
} {
    Callback that is executed to find the package_file, depending on system
} -

ad_proc -public -callback intranet-translation::before_project_create {
    -project_type_id:required
    -company_id:required
} {
    Callback that is executed before the project is created to e.g. set missing project_nr and project_name
} -

ad_proc -public -callback intranet-translation::project_info {
    -project_id:required
} {
    Callback that is executed during display of the project_info portlet after all the standard options.    
} -

ad_proc -public -callback im_category_after_create -impl trans_tasks {
    -object_id:required
    -status:required 
    -type:required
    -category_id:required
    -category_type:required
} {
    Ensure the _end_date and _id columns exist for the aux_string1 workflow steps
} {
    if {$category_type eq "Intranet Project Type"} {
	# We only check on the Intranet Project Type
	set workflow_steps [db_string workflow_steps "select aux_string1 from im_categories where category_id = :category_id" -default ""]
	foreach workflow_step $workflow_steps {
	    if {![im_column_exists im_trans_tasks ${workflow_step}_id]} {
		db_dml add_id_column "alter table im_trans_tasks add column ${workflow_step}_id integer references users"
	    }
	    if {![im_column_exists im_trans_tasks ${workflow_step}_end_date]} {
		db_dml add_id_column "alter table im_trans_tasks add column ${workflow_step}_end_date timestamptz"
	    }
	}
    }
}

ad_proc -public -callback im_category_after_update -impl trans_tasks {
    -object_id:required
    -status:required 
    -type:required
    -category_id:required
    -category_type:required
} {
    Ensure the _end_date and _id columns exist for the aux_string1 workflow steps
} {
    if {$category_type eq "Intranet Project Type"} {
	# We only check on the Intranet Project Type
	set workflow_steps [db_string workflow_steps "select aux_string1 from im_categories where category_id = :category_id" -default ""]
	foreach workflow_step $workflow_steps {
	    if {![im_column_exists im_trans_tasks ${workflow_step}_id]} {
		db_dml add_id_column "alter table im_trans_tasks add column ${workflow_step}_id integer references users"
	    }
	    if {![im_column_exists im_trans_tasks ${workflow_step}_end_date]} {
		db_dml add_id_column "alter table im_trans_tasks add column ${workflow_step}_end_date timestamptz"
	    }
	}
    }
}
