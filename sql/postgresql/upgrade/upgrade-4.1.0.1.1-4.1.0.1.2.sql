SELECT acs_log__debug('/packages/intranet-translation/sql/postgresql/upgrade/upgrade-4.1.0.1.1-4.1.0.1.2.sql','');

-- Add the certified level in case it doesn't exist yet
SELECT im_category_new ('2204', 'Certified', 'Intranet Experience Level');


-- Add a new project type for certified translation

SELECT im_category_new ('2505', 'Certified Translation', 'Intranet Project Type');
update im_categories set aux_string1 = 'cert', category_description = 'Certified Translation project requiring certified translators' where category_id = 2505; 

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from im_category_hierarchy where child_id = 2505 and parent_id= 2500;

    IF 0 != v_count THEN return 0; END IF;

    insert into im_category_hierarchy (child_id,parent_id) values (2505,2500);

    -- Add the dynfields in the type map
    insert into im_dynfield_type_attribute_map select attribute_id, 2505, display_mode,help_text,section_heading,default_value,required_p,help_url from im_dynfield_type_attribute_map where object_type_id = 93;
    return 1;
end;
$$ LANGUAGE 'plpgsql';
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

-- Add the cert task type
SELECT im_category_new ('4218', 'cert', 'Intranet Trans Task Type');
update im_categories set aux_int1 = (select aux_int1 from im_categories where category_id = 4210), aux_int2 = 2505 where category_id = 4218;

-- Alter trans tasks to support cert
create or replace function inline_0 ()
returns integer as '
declare
        v_count         integer;
begin

        select count(*) into v_count from information_schema.columns where
              table_name = ''im_trans_tasks''
              and column_name = ''cert_end_date'';
        IF v_count > 0 THEN return 1; END IF;
        alter table im_trans_tasks add column cert_end_date timestamptz;
        RETURN 0;

end;' language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

create or replace function inline_0 ()
returns integer as '
declare
        v_count         integer;
begin

        select count(*) into v_count from information_schema.columns where
              table_name = ''im_trans_tasks''
              and column_name = ''cert_id'';
        IF v_count > 0 THEN return 1; END IF;
	alter table im_trans_tasks add column cert_id integer references users(user_id);
        RETURN 0;

end;' language 'plpgsql';
select inline_0 ();
drop function inline_0 ();




