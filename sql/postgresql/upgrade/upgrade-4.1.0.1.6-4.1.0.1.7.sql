SELECT acs_log__debug('/packages/intranet-translation/sql/postgresql/upgrade/upgrade-4.1.0.1.6-4.1.0.1.7.sql','');

-- Processing Time

create or replace function inline_0 ()
returns integer as $body$
declare
        v_count  integer;
begin
        select count(*) into v_count from user_tab_columns
    where lower(table_name) = 'im_projects' and lower(column_name) = 'processing_time';

        IF v_count = 0 THEN
               alter table im_projects add column processing_time float8;
              perform im_dynfield_attribute_new(
                  'im_project',
                  'processing_time',
                  'Days to process the project',
                  'numeric',
                  'float',
                  'f',
                  '10',
                  'f',
                  'im_projects'
                  );
        END IF;
        return 0;
end;$body$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

-- Alter trans tasks to support cert
create or replace function inline_0 ()
returns integer as '
declare
        v_count         integer;
begin

        select count(*) into v_count from information_schema.columns where
              table_name = ''im_trans_tasks''
              and column_name = ''copy_end_date'';
        IF v_count > 0 THEN return 1; END IF;
        alter table im_trans_tasks add column copy_end_date timestamptz;
        RETURN 0;

end;' language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

create or replace function inline_0 ()
returns integer as '
declare
        v_count         integer;
begin

        select count(*) into v_count from information_schema.columns where
              table_name = ''im_trans_tasks''
              and column_name = ''copy_id'';
        IF v_count > 0 THEN return 1; END IF;
        alter table im_trans_tasks add column copy_id integer references users(user_id);
        RETURN 0;

end;' language 'plpgsql';
select inline_0 ();
drop function inline_0 ();
