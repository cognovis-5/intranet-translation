-- upgrade-4.1.0.0.4-4.1.0.0.5.sql
-- Copyright (c) 2017, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- @author Malte Sussdorff (malte.sussdorff@cognovis.de)
-- @creation-date 2017-07-26
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/intranet-translation/sql/postgresql/upgrade/upgrade-4.1.0.0.4-4.1.0.0.5.sql','');

-- Add component on projects
SELECT im_component_plugin__new (
        null,                           -- plugin_id
        'acs_object',                   -- object_type
        now(),                          -- creation_date
        null,                           -- creation_user
        null,                           -- creation_ip
        null,                           -- context_id
        'Intranet Project Trados Folder Component',        -- plugin_name
        'intranet-translation',                  -- package_name
        'right',                        -- location
        '/intranet/projects/view',      -- page_url
        null,                           -- view_name
        10,                             -- sort_order
        'im_trans_trados_folder_component -user_id $user_id -project_id $project_id -return_url $return_url',
    'lang::message::lookup "" intranet-translation.Project_Trados_Folder "Project Trados Folder"'
);


-- Make the component readable for employees and poadmins
CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS integer AS '
DECLARE

    v_object_id	integer;
    v_employees	integer;
    v_poadmins	integer;

BEGIN
    SELECT group_id INTO v_poadmins FROM groups where group_name = ''P/O Admins'';

    SELECT group_id INTO v_employees FROM groups where group_name = ''Employees'';

    SELECT plugin_id INTO v_object_id FROM im_component_plugins WHERE plugin_name = ''Intranet Project Trados Folder Component'';

    PERFORM im_grant_permission(v_object_id,v_employees,''read'');
    PERFORM im_grant_permission(v_object_id,v_poadmins,''read'');


    RETURN 0;

END;' language 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

    