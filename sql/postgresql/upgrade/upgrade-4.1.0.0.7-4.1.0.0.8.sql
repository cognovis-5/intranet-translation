-- upgrade-4.1.0.0.5-4.1.0.0.6.sql
-- Copyright (c) 2017, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- @author Malte Sussdorff (malte.sussdorff@cognovis.de)
-- @creation-date 2017-07-26
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/intranet-translation/sql/postgresql/upgrade/upgrade-4.1.0.0.7-4.1.0.0.8.sql','');

-- Mark Subject Area as a skill with a link to a table in im_projects
update im_categories set aux_string2 = 'subject_area_id' where aux_string1 = 'Intranet Translation Subject Area' and category_type = 'Intranet Skill Type';
	
-- Update the projects to add the subject_area_id as a skill to the project

create or replace function inline_0()
RETURNS integer AS '
DECLARE

	row		record;
	v_skill_type_id 	integer;
	v_exists_p	integer;
BEGIN

	SELECT category_id INTO v_skill_type_id FROM im_categories where aux_string1 = ''Intranet Translation Subject Area'' and category_type = ''Intranet Skill Type'';

	FOR row IN
			select  project_id, subject_area_id
			from    im_projects p
			where  subject_area_id is not null
	LOOP
	
select count(*) into v_exists_p from im_object_freelance_skill_map where object_id = row.project_id and skill_type_id = v_skill_type_id and skill_id = row.subject_area_id;
		
		IF v_exists_p = 0 THEN
			insert into im_object_freelance_skill_map (
				object_skill_map_id,
				object_id,
				skill_type_id,
				skill_id,
				required_experience_id,
				skill_weight,
				skill_required_p
			) values (
			nextval(''im_freelance_object_skill_seq''),
				row.project_id,
				v_skill_type_id,
				row.subject_area_id,
				null,
				null,
				''t''
			);
		END IF;
	END LOOP;


	RETURN 0;

END;' language 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();


	