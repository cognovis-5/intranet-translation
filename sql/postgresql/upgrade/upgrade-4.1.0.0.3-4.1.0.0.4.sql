-- upgrade-4.1.0.0.3-4.1.0.0.4.sql
-- Copyright (c) 2017, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- @author Malte Sussdorff (malte.sussdorff@cognovis.de)
-- @creation-date 2017-07-26
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/intranet-translation/sql/postgresql/upgrade/upgrade-4.1.0.0.3-4.1.0.0.4.sql','');

-- 4210-4220    Intranet Trans Task Type


SELECT im_category_new (4210,'Trans', 'Intranet Trans Task Type');
SELECT im_category_new (4211,'Edit', 'Intranet Trans Task Type');
SELECT im_category_new (4212,'Proof', 'Intranet Trans Task Type');
SELECT im_category_new (4213,'Other', 'Intranet Trans Task Type');
SELECT im_category_new (4219,'PM', 'Intranet Trans Task Type');

update im_categories set aux_int1=1500 where category_id = 4210;
update im_categories set aux_int1=4500 where category_id = 4211;
update im_categories set aux_int1=3000 where category_id = 4219;